package fr.eurecom.android.implicitintent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    //protected Uri mediaFileURI = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }
        return mediaFile;
    }

    public void callintent(View view) {
        Intent intent = null;
        switch(view.getId())
        {
            case R.id.callbrowser:
                EditText textBrow = (EditText) findViewById(R.id.callBrowsertxt);
                String url = textBrow.getText().toString();
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+url));
                startActivity(intent);
                break;
            case R.id.callsomeone:
                EditText textBrow2 = (EditText) findViewById(R.id.callsomeonetxt);
                String number = textBrow2.getText().toString();
                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + number));
                try {
                    startActivity(intent);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
                break;
            case R.id.dial:
                intent = new Intent(Intent.ACTION_DIAL);
                try {
                    startActivity(intent);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
                break;
            case R.id.showmap:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:43.6, 7.0?z=11"));
                try {
                    startActivity(intent);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
                break;
            case R.id.searchmap:
                EditText txt = (EditText) findViewById(R.id.searchmaptxt);
                String place = txt.getText().toString();
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:0,0?q=" + place));
                try {
                    startActivity(intent);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
                break;
            case R.id.takeapicture:
                // create Intent to take a picture and return control to the calling application
                Intent intent5 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
                Toast.makeText(this,"FileUri: " + fileUri.toString(),Toast.LENGTH_LONG);
                Log.i("imageCaptureIntent", "FILEURI: " + fileUri.toString());
                intent5.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

                // start the image capture Intent
                startActivityForResult(intent5, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                /*try {
                    startActivity(intent5);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }*/
                break;
            case R.id.showcontact:
                intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                try {
                    startActivity(intent);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
                break;
            case R.id.editthefirstcontact:
                Intent i = new Intent(Intent.ACTION_EDIT);
                i.setData(Uri.parse("content://contacts/people/1"));

                try {
                    startActivity(i);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
                break;
        }

    }
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        //File f = new File(mCurrentPhotoPath);
        Uri contentUri = fileUri; //Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        Log.i("galleryAddPic","sent");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Image saved!",Toast.LENGTH_LONG).show();
                galleryAddPic();
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }

        if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Video captured and saved to fileUri specified in the Intent
                Toast.makeText(this, "Video saved to:\n" +
                        data.getData(), Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the video capture
            } else {
                // Video capture failed, advise user
            }
        }
    }
}
